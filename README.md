# Dockerfile(s) for build Basalt packages

<!--
Copyright 2021-2023, Collabora, Ltd.
SPDX-License-Identifier: CC-BY-4.0
-->

## Usage

```bash
./build.sh
./copy.sh
```
