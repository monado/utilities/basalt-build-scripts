#!/usr/bin/bash
# Copyright 2022-2023, Collabora, Ltd.
# SPDX-License-Identifier: MIT or BSL-1.0 or Apache-2.0

SCRIPT_DIR=$(dirname $(readlink -f $0))

docker build \
	-t basalt-base-ubuntu-2204:latest \
	-f "docker/Dockerfile.Base.Ubuntu-22.04" \
	"$SCRIPT_DIR/docker"
