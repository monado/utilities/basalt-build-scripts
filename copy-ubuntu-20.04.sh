#!/usr/bin/bash
# Copyright 2022-2023, Collabora, Ltd.
# SPDX-License-Identifier: MIT or BSL-1.0 or Apache-2.0

docker run --name temp-basalt-ubuntu-2004 basalt-ubuntu-2004:latest
docker cp temp-basalt-ubuntu-2004:"/root/basalt/basalt-ubuntu-2004.deb" .
docker rm temp-basalt-ubuntu-2004
