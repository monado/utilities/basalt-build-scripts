#!/usr/bin/bash
# Copyright 2022-2023, Collabora, Ltd.
# SPDX-License-Identifier: MIT or BSL-1.0 or Apache-2.0

SCRIPT_DIR=$(dirname $(readlink -f $0))

"$SCRIPT_DIR/build-ubuntu-20.04.sh"
"$SCRIPT_DIR/build-ubuntu-22.04.sh"
